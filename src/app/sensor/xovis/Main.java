package app.sensor.xovis;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            String fileName = new java.io.File( "." ).getCanonicalPath()
                    + "/src/app/sensor/xovis/"
                    + "coordinate-push_00-1E-C0-9C-EC-07_2017-11-07-10-33-40";

            PersonCoordinatePush.CoordinatesFrame frame = PersonCoordinatePush.CoordinatesFrame.parseFrom(
                new FileInputStream(fileName)
            );

            List coords = frame.getCoordinatesList();
            Long timestamp = frame.getTimestamp();

            System.out.println(coords);
            System.out.println(timestamp);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
